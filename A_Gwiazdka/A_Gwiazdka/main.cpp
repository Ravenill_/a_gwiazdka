#include <iostream>
#include <cstdlib>
#include "AStar.h"
#include <queue>
#include <vector>
#include "Comparator.h"

int main()
{
	AStar gwiazdka(static_cast<std::string>("a.txt"));
	gwiazdka.printMap();
	gwiazdka.runPathFinder(20, 10);
	std::cout << "\n\n";
	gwiazdka.printMap();
	gwiazdka.printPath();

	/*
	std::priority_queue<Node*, std::vector<Node*>, NodeComparator> OL;
	Node * l = new Node;
	Node *kutaz = nullptr;

	OL.push(l);
	//kutaz = OL.top();

	//std::cout << (*kutaz).can_go;
	//OL.pop();
	*/
	system("PAUSE");
}