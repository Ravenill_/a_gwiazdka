#pragma once
#include "Node.h"
#include <string>
#include <queue>
#include <vector>
#include "Comparator.h"

class AStar
{
private:
	//map
	Node **_map;
	int _map_size_x;
	int _map_size_y;

	Node *_start;
	Node *_end;

	bool isIn(Node*, std::priority_queue<Node*, std::vector<Node*>, NodeComparator>);
	bool isIn(Node*, std::vector<Node*>);
	void calculate(Node&, int, int);
	int calculateG(Node, int, int);
	void calculateF(Node&);
	void findPath();
	void reset();

public:
	AStar(std::string);
	~AStar();

	void printMap();
	void printPath();
	void runPathFinder(int, int);
};